import Swift

// 코드 부록-21 프로그램 진입 지점을 나타내는 TopLevel 구조체

@main
struct TopLevel {
    static func main() {
        // Top-level code goes here
    }
}
